<?php
/**
 * @file
 * Theme settings.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function wheke_responsive_theme_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['wheke_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Wheke Theme Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['wheke_settings']['breadcrumbs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show breadcrumbs in a page'),
    '#default_value' => theme_get_setting('breadcrumbs', 'wheke_responsive_theme'),
    '#description'   => t("Check this option to show breadcrumbs in page. Uncheck to hide."),
  );
}
